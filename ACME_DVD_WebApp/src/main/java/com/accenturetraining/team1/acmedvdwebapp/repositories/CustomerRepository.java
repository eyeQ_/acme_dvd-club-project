package com.accenturetraining.team1.acmedvdwebapp.repositories;

import com.accenturetraining.team1.acmedvdwebapp.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Optional<Customer> getById(long id);

    List<Customer> getAllByOrderByTotalPaymentsDesc();
    
//    OrByIdentificationNumberOrByMobileNumberOrByHomeNumberOrByEmail
    Optional<List<Customer>> findAllByFirstNameContainingAndLastNameContainingAndAddressContainingAndMobileNumberContainingAndHomeNumberContainingAndEmailContainingAndIdentificationNumberContaining
            (String firstName, String lastName, String address, String mobileNumber,
             String homeNumber, String email, String identificationNumber);
}