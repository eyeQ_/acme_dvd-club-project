package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.dtos.ActorDTO;
import com.accenturetraining.team1.acmedvdwebapp.dtos.FilmDetailsDTO;
import com.accenturetraining.team1.acmedvdwebapp.dtos.FilmRentalCountDTO;
import com.accenturetraining.team1.acmedvdwebapp.dtos.FilmRequestBodyDTO;
import com.accenturetraining.team1.acmedvdwebapp.model.Actor;
import com.accenturetraining.team1.acmedvdwebapp.model.Copy;
import com.accenturetraining.team1.acmedvdwebapp.model.Film;
import com.accenturetraining.team1.acmedvdwebapp.model.Genre;
import com.accenturetraining.team1.acmedvdwebapp.repositories.ActorRepository;
import com.accenturetraining.team1.acmedvdwebapp.repositories.CopyRepository;
import com.accenturetraining.team1.acmedvdwebapp.repositories.FilmRepository;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class FilmServicesImpl implements FilmServices {
    
    @Autowired
    FilmRepository filmRepository;
    @Autowired
    CopyRepository copyRepository;
    @Autowired
    CopyServices copyServices;
    @Autowired
    ActorRepository actorRepository;
    @Autowired
    ActorServices actorServices;
    @Autowired
    GenreServices genreServices;
    @Autowired
    FilmDataDeserializer filmDataDeserializer;
    @Autowired
    RentalServices rentalServices;
    
    @Override
    public FilmDetailsDTO getFilmDetails(long filmId) {
        FilmDetailsDTO filmDetailsDTO = new FilmDetailsDTO();
        Film film = getFilmById(filmId);
        filmDetailsDTO.setId(filmId);
        filmDetailsDTO.setTitle(film.getTitle());
        filmDetailsDTO.setReleaseDate(film.getReleaseDate());
        filmDetailsDTO.setDirector(film.getDirector());
        filmDetailsDTO.setDescription(film.getDescription());
        
        //Save genres as single string.
        if (film.getGenres() != null && !film.getGenres().isEmpty()) {
            List<Genre> genres = film.getGenres();
            StringBuilder genresText = new StringBuilder();
            genresText.append(genres.get(0).getGenreName());
            for (int i = 1; i < genres.size(); i++) {
                genresText.append(", " + genres.get(i).getGenreName());
            }
            filmDetailsDTO.setGenres(genresText.toString());
        }
        
        // Save actors as single string.
        if (film.getActors() != null && !film.getActors().isEmpty()) {
            List<Actor> actors = film.getActors();
            StringBuilder actorsText = new StringBuilder();
            actorsText.append(actors.get(0).getFirstName()
                    + " " + actors.get(0).getLastName());
            for (int i = 1; i < film.getActors().size(); i++) {
                actorsText.append(", " + actors.get(i).getFirstName()
                        + " " + actors.get(i).getLastName());
            }
            filmDetailsDTO.setActors(actorsText.toString());
        }
        
        return filmDetailsDTO;
    }
    
    @Override
    public Film getFilmById(long filmId) {
        Film film = filmRepository.getById(filmId)
                .orElseThrow(() -> new FilmNotFoundException(filmId));
        return film;
    }
    
    @Override
    public List<FilmDetailsDTO> getAllFilms() {
        List<Film> allFilms = filmRepository.findAll();
        List<FilmDetailsDTO> filmDetailsDTOS = new ArrayList<>();
        for (Film film : allFilms) {
            filmDetailsDTOS.add(getFilmDetails(film.getId()));
        }
        return filmDetailsDTOS;
    }
    
    @Override
    public long registerNewFilm(FilmRequestBodyDTO filmRequestBodyDTO) {
        Film newFilm = convertDTOToFilm(filmRequestBodyDTO);
        log.info("A NEW FILM WAS CREATED");
        return newFilm.getId();
    }
    
    @Override
    public Film convertDTOToFilm(FilmRequestBodyDTO filmRequestBodyDTO) {
        Film film = new Film();
        mapDTOToFilm(filmRequestBodyDTO, film);
        return film;
    }
    
    @Override
    public Film updateFilm(Film film) {
        film = filmRepository.saveAndFlush(film);
        return film;
    }
    
    @Override
    public void editFilm(long filmId, FilmRequestBodyDTO filmRequestBodyDTO) {
        Film film = filmRepository.getById(filmId)
                .orElseThrow(() -> new FilmNotFoundException(filmId));
        mapDTOToFilm(filmRequestBodyDTO, film);
    }
    
    private Film mapDTOToFilm(FilmRequestBodyDTO filmRequestBodyDTO, Film film) {
        film.setTitle(filmRequestBodyDTO.getTitle());
        film.setDirector(filmRequestBodyDTO.getDirector());
        film.setDescription(filmRequestBodyDTO.getDescription());
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate releaseDate = LocalDate.parse(filmRequestBodyDTO.getReleaseDate(), dateFormat);
        film.setReleaseDate(releaseDate);
        List<String> genreNamesList = filmRequestBodyDTO.getGenres();
        List<Genre> filmGenres = new ArrayList<>();
        film.setGenres(filmGenres);
        film = updateFilm(film);
        for (String genreName : genreNamesList) {
            filmGenres.add(filmDataDeserializer.saveGenreFromJson(genreName, film));
        }
        List<ActorDTO> actorDTOS = filmRequestBodyDTO.getActors();
        List<Actor> filmActors = new ArrayList<>();
        film.setActors(filmActors);
        for (ActorDTO actorDTO : actorDTOS) {
            filmActors.add(filmDataDeserializer.saveActorFromJson(actorDTO, film));
        }
        film.setGenres(filmGenres);
        film.setActors(filmActors);
        film = filmRepository.saveAndFlush(film);
        return film;
    }
    
    // SEARCHES
    @Override
    public List<FilmDetailsDTO> findFilmsByTitlePart(String titlePart) throws FilmNotFoundException {
        log.debug("SEARCHING FOR FILM WITH A TITLE THAT CONTAINS " + titlePart);
        List<Film> films = filmRepository.findAllByTitleContaining(titlePart)
                .orElseThrow(() -> new FilmNotFoundException(titlePart));
        log.debug("FOUND THE LIST");
        List<FilmDetailsDTO> filmDetailsDTOS = new ArrayList<>();
        for (Film film : films) {
            filmDetailsDTOS.add(getFilmDetails(film.getId()));
        }
        
        return filmDetailsDTOS;
    }

    
    @Override
    public List<FilmDetailsDTO> findFilmsByReleaseDate(String dateFromText, String dateToText)
            throws FilmNotFoundException {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate dateFrom = LocalDate.parse(dateFromText, dateFormat);
        LocalDate dateTo = LocalDate.parse(dateToText, dateFormat);
        List<Film> films = filmRepository.findAllByReleaseDateBetween(dateFrom, dateTo)
                .orElseThrow(() -> new FilmNotFoundException(dateFrom, dateTo));
        List<FilmDetailsDTO> filmDetailsDTOS = new ArrayList<>();
        for (Film film : films) {
            filmDetailsDTOS.add(getFilmDetails(film.getId()));
        }
        return filmDetailsDTOS;
    }
    
    @Override
    public List<FilmDetailsDTO> findFilmsByGenre(String genreNamePart) {
        List<FilmDetailsDTO> filmDetailsDTOS = new ArrayList<>();
        List<Film> films = filmRepository.searchFilmsByGenre(genreNamePart)
                .orElseThrow(() -> new FilmNotFoundForGenreException(genreNamePart));
        for (Film film : films) {
            filmDetailsDTOS.add(getFilmDetails(film.getId()));
        }
        return filmDetailsDTOS;
    }
    
    @Override
    public List<FilmDetailsDTO> findFilmsByActor(String actorFirstNamePart, String actorLastNamePart) {
        List<FilmDetailsDTO> filmDetailsDTOS = new ArrayList<>();
        List<Film> films = filmRepository.searchFilmsByActor(actorFirstNamePart, actorLastNamePart)
                .orElseThrow(() -> new FilmNotFoundForActorException(actorFirstNamePart, actorLastNamePart));
        for (Film film : films) {
            filmDetailsDTOS.add(getFilmDetails(film.getId()));
        }
        return filmDetailsDTOS;
    }
    
    @Override
    public List<FilmDetailsDTO> findAvailableFilmsByTitlePart(String titlePart) throws FilmNotFoundException {
        List<Film> films = filmRepository.findAllByTitleContaining(titlePart)
                .orElseThrow(() -> new FilmNotFoundException(titlePart));
        List<Film> availableFilms = new ArrayList<>();
        for (Film film : films) {
            if (copyServices.checkForFilmAvailability(film.getId())) {
                availableFilms.add(film);
            }
        }
        if (availableFilms.isEmpty()) {
            throw new FilmNotFoundException();
        }
        List<FilmDetailsDTO> filmDetailsDTOS = new ArrayList<>();
        for (Film availableFilm : availableFilms) {
            filmDetailsDTOS.add(getFilmDetails(availableFilm.getId()));
        }
        return filmDetailsDTOS;
    }
    
    
//    @Override
//    public List<FilmDetailsDTO> findFilmsByReleaseDate(String dateFromText, String dateToText)
//            throws FilmNotFoundException {
//        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM/dd/yyyy");
//        LocalDate dateFrom = LocalDate.parse(dateFromText, dateFormat);
//        LocalDate dateTo = LocalDate.parse(dateToText, dateFormat);
//        List<Film> films = filmRepository.findAllByReleaseDateBetween(dateFrom, dateTo)
//                .orElseThrow(() -> new FilmNotFoundException(dateFrom, dateTo));
//        List<FilmDetailsDTO> filmDetailsDTOS = new ArrayList<>();
//        for (Film film : films) {
//            filmDetailsDTOS.add(getFilmDetails(film.getId()));
//        }
//        return filmDetailsDTOS;
//    }
//
//    @Override
//    public List<FilmDetailsDTO> findFilmsByGenre(String genreNamePart) {
//        List<FilmDetailsDTO> filmDetailsDTOS = new ArrayList<>();
//        List<Film> films = filmRepository.searchFilmsByGenre(genreNamePart)
//                .orElseThrow(() -> new FilmNotFoundForGenreException(genreNamePart));
//        for (Film film : films) {
//            filmDetailsDTOS.add(getFilmDetails(film.getId()));
//        }
//        return filmDetailsDTOS;
//    }
//
//    @Override
//    public List<FilmDetailsDTO> findFilmsByActor(String actorFirstNamePart, String actorLastNamePart) {
//        List<FilmDetailsDTO> filmDetailsDTOS = new ArrayList<>();
//        List<Film> films = filmRepository.searchFilmsByActor(actorFirstNamePart, actorLastNamePart)
//                .orElseThrow(() -> new FilmNotFoundForActorException(actorFirstNamePart, actorLastNamePart));
//        for (Film film : films) {
//            filmDetailsDTOS.add(getFilmDetails(film.getId()));
//        }
//        return filmDetailsDTOS;
//    }
    
    
    @Override
    public void validateNewFilm(FilmDetailsDTO filmDetailsDTO) throws FilmDataIncompleteException, FilmDuplicateException {
//        String filmTitle = filmDetailsDTOm.getTitle();
//        int year = filmDetailsDTOm.getReleaseDate().getYear();
//
//        // Check if the title is null or empty.
//        if (filmDetailsDTOm.getTitle() == null || filmDetailsDTOm.getTitle() == "") {
//            throw new FilmDataIncompleteException("title");
//        }
//
//        // Check if the release date is null.
//        if (filmDetailsDTOm.getReleaseDate() == null) {
//            throw new FilmDataIncompleteException("release date");
//        }
//
//        // Check if any films with the same title exist.
//        List<Film> filmsWithSameTitle = filmRepository.getByTitleEquals(filmTitle).orElse(null);
//
//        // If any such films exist, check that their releaseDate of release is different.
//        if (filmsWithSameTitle != null) {
//            List<Film> filmsWithSameTitleAndYear = filmsWithSameTitle.stream()
//                    .filter(filmWithSameTitle -> filmWithSameTitle
//                            .getReleaseDate().getYear() == year).collect(Collectors.toList());
//            if (!filmsWithSameTitleAndYear.isEmpty()) {
//                throw new FilmDuplicateException(filmTitle, year);
//            }
//        }
    }
    
    @Override
    public List<FilmDetailsDTO> getAllUnavailableFilms() {
        List<FilmDetailsDTO> filmDetailsDTOS = new ArrayList<>();
        List<Film> rentedFilms = filmRepository.findRentedFilms()
                .orElseThrow(() -> new FilmNotFoundException());
        List<Film> unavailableFilms = new ArrayList<>();
        for (Film rentedFilm : rentedFilms) {
            if (!copyServices.checkForFilmAvailability(rentedFilm.getId())) {
                unavailableFilms.add(rentedFilm);
            }
        }
        for (Film unavailableFilm : unavailableFilms) {
            filmDetailsDTOS.add(getFilmDetails(unavailableFilm.getId()));
        }
        
        return filmDetailsDTOS;
    }
    
    @Override
    public List<FilmRentalCountDTO> getBestSellingFilms(){
        Map<Film, Integer> filmMap = new HashMap<>();
        for(Copy copy : copyRepository.findAllByRentalsNotNull().get()){
            Film film = copy.getFilm();
            int copyRentalCount = copy.getRentals().size();
            if (copyRentalCount == 0) {
                continue;
            }
            if (filmMap.containsKey(film)) {
                int filmRentalCount = filmMap.get(film);
                filmMap.put(film, filmRentalCount + copyRentalCount);
            }
            else {
                filmMap.put(film, copyRentalCount);
            }
        }
        return rentalServices.convertMapToFilmRentalCountDTO(filmMap);
    }
    
    
}