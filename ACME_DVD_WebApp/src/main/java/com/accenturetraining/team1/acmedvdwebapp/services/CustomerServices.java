package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.dtos.CustomerDetailsDTO;
import com.accenturetraining.team1.acmedvdwebapp.dtos.CustomerRequestBodyDTO;
import com.accenturetraining.team1.acmedvdwebapp.model.Customer;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.CustomerAccountIsInactive;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.CustomerInDebtException;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.CustomerDuplicateException;

import java.util.List;

public interface CustomerServices {
    /**
     * Finds all customers and
     * @return them in a list
     */
    List<CustomerDetailsDTO> getAllCustomers();

    /**
     * Sets customer inactive
     * @param id
     * @return
     * @throws CustomerInDebtException
     */
    Customer setCustomerInactive(long id) throws CustomerInDebtException;
    
    /**
     * Checks if a customer with the given id exists.
     * @param customerId
     * @return customer
     */
    public Customer findCustomerById(long customerId);
    
//    void validateCustomerForRegistration(Customer customer) throws CustomerDuplicateException;
    
    /**
     * Checks the database for a customer with the the given unique fields
     * and, if it doesn't find one, it returns a customer with only those
     * fields filled. If it does, it returns the already existing customer.
     * @throws CustomerDuplicateException
     */
    Customer registerCustomer(CustomerRequestBodyDTO customerRequestBodyDTO) throws CustomerDuplicateException;

    /**
     * adds a customer to the data base given the following parameters
     * @param firstName
     * @param lastName
     * @param identificationNumber
     * @param address
     * @param mobileNumber
     * @param email
     * @return that customer
     */
    Customer addCustomer(String firstName, String lastName, String identificationNumber, String address,
                         String mobileNumber, String email);


    CustomerDetailsDTO convertCustomerToDTO(Customer customer);
    
    /**
     * Checks for Customers Debt
     * @param customer
     * @throws CustomerInDebtException
     */
    void validateCustomerDebt(Customer customer) throws CustomerInDebtException;

    /**
     * checks if a customer account is active
     * @param customer
     * @throws CustomerAccountIsInactive exception if he is not
     */
    void checkIfCustomerIsActive(Customer customer) throws CustomerAccountIsInactive;

    /**
     * updates the customer account
     * @param customer
     * @return the customer account
     * @throws CustomerDuplicateException
      */
    public Customer updateCustomer(Customer customer) throws CustomerDuplicateException;

    /**
     * Edit the customer account with the given
     * @param customerId and
     * @param customerRequestBodyDTO parameters
     * @throws CustomerDuplicateException
     */
    void editCustomer(long customerId, CustomerRequestBodyDTO customerRequestBodyDTO) throws CustomerDuplicateException;

    /**
     * finds the top20 customers based on total payments
     * @return a list of top20 customers
     */
    public List<Customer> getTopCustomersByTotalPayments();

    /**
     * searches for customers that fulfill one of the given parameters
     * @param customerRequestBodyDTO
     * @return a list of customers
     */
    List<CustomerDetailsDTO> findCustomersByDetails(CustomerRequestBodyDTO customerRequestBodyDTO);
}
