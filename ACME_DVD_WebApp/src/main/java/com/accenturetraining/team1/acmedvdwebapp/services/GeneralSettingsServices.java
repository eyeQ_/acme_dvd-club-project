package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.model.GeneralSettings;

public interface GeneralSettingsServices {
    
    GeneralSettings registerNewSettingsProfile(GeneralSettings generalSettings);
    
    GeneralSettings updateGeneralSettings(GeneralSettings generalSettings);
    
    GeneralSettings findGeneralSettingsById(int id);
}
