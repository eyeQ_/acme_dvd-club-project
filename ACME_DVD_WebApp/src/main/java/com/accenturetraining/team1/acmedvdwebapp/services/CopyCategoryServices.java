package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.model.CopyCategory;

public interface CopyCategoryServices {

    /**
     * Register a copy category
     * @param copyCategory
     * @return
     */
    public long registerCopyCategory(CopyCategory copyCategory);

    /**
     * Used for finding the copy category by id
     * @param copyCategoryId
     * @return
     */
    CopyCategory findCopyCategoryById(int copyCategoryId);
}
