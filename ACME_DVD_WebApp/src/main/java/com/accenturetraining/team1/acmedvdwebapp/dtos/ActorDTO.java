package com.accenturetraining.team1.acmedvdwebapp.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data Transfer Object
 * Used for mapping actor's name
 */

@Data
@NoArgsConstructor
@AllArgsConstructor

public class ActorDTO {
    private String firstName;
    private String lastName;
}
//for mapping actor's name