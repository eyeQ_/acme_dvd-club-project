package com.accenturetraining.team1.acmedvdwebapp.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * Data Transfer Object
 * Used as a response body to inform by providing rental information
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RentalDetailsDTO {
    private long rentalId;
    private long customerId;
    private LocalDateTime rentalDate;
    private LocalDateTime returnDate;
    private double totalCharge;
    private long filmId;
    private long copyId;
}
