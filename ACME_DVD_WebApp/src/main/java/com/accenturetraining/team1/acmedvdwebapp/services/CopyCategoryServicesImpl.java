package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.model.CopyCategory;
import com.accenturetraining.team1.acmedvdwebapp.repositories.CopyCategoryRepository;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.CopyCategoryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CopyCategoryServicesImpl implements CopyCategoryServices{
    @Autowired
    CopyCategoryRepository copyCategoryRepository;
    
    @Override
    public long registerCopyCategory(CopyCategory copyCategory) {
        CopyCategory storedCopyCategory = copyCategoryRepository.saveAndFlush(copyCategory);
        return storedCopyCategory.getId();
    }
    
    @Override
    public CopyCategory findCopyCategoryById(int copyCategoryId) {
        CopyCategory copyCategory = copyCategoryRepository.getById(copyCategoryId)
                .orElseThrow(() -> new CopyCategoryNotFoundException(copyCategoryId));
        return copyCategory;
    }
}
