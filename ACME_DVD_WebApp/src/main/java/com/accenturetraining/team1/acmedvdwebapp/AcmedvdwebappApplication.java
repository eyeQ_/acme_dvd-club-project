package com.accenturetraining.team1.acmedvdwebapp;

import com.accenturetraining.team1.acmedvdwebapp.services.CustomerServicesImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class AcmedvdwebappApplication {
  
  public static void main(String[] args) {
    SpringApplication.run(AcmedvdwebappApplication.class, args);
  }
}
