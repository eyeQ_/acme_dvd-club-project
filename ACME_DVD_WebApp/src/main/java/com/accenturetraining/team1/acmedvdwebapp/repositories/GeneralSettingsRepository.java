package com.accenturetraining.team1.acmedvdwebapp.repositories;

import com.accenturetraining.team1.acmedvdwebapp.model.GeneralSettings;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface GeneralSettingsRepository extends JpaRepository<GeneralSettings, Integer> {
    Optional<GeneralSettings> getById(int id);
}
