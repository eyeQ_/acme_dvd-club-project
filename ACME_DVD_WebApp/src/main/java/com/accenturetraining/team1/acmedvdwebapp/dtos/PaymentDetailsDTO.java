package com.accenturetraining.team1.acmedvdwebapp.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * Data Transfer Object
 * Used as a response body to inform that the payment is done by providing payment information about the customer
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaymentDetailsDTO {
    private long customerId;
    private LocalDateTime paymentDateAndTime;
    private double amountPaid;
    private double updatedCustomerDebt;
}
//respose body to inform that the payment is done with these details