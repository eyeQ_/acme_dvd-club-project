package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.dtos.FilmDetailsDTO;
import com.accenturetraining.team1.acmedvdwebapp.dtos.FilmRentalCountDTO;
import com.accenturetraining.team1.acmedvdwebapp.dtos.FilmRequestBodyDTO;
import com.accenturetraining.team1.acmedvdwebapp.model.Film;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.FilmDataIncompleteException;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.FilmDuplicateException;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.FilmNotFoundException;

import java.util.List;

public interface FilmServices {

    /**
     * Returns the film details
     * @param filmId
     * @return
     */
    FilmDetailsDTO getFilmDetails(long filmId);

    /**
     * Returns the film with the given film id
     * @param filmId
     * @return film
     */
    Film getFilmById(long filmId);

    /**
     * Finds all films
     * @return a list of films
     */
    List<FilmDetailsDTO> getAllFilms();

    /**
     * Registers a new film to the data base
     * @param filmRequestBodyDTO
     * @return the new film
     */
    long registerNewFilm(FilmRequestBodyDTO filmRequestBodyDTO);

    /**
     * Converts the given filRequestBodyDto to a film entity
     * @param filmRequestBodyDTO
     * @return the film entity
     */
    Film convertDTOToFilm(FilmRequestBodyDTO filmRequestBodyDTO);

    /**
     * Saves a film to the data base
     * @param film
     * @return that film
     */
    Film updateFilm(Film film);

    /**
     * Edit a film with a given id
     * @param filmId
     * @param editedFilm
     */
    void editFilm(long filmId, FilmRequestBodyDTO editedFilm);
    
    /**
     * Searches for films by a title
     * @param title
     * @return the list of films
     * @throws FilmNotFoundException
     */
    List<FilmDetailsDTO> findFilmsByTitlePart(String title) throws FilmNotFoundException;

    /**
     * Finds the available films by the given title
     * @param titlePart
     * @return a list of films if it matches the title part
     * @throws FilmNotFoundException
     */
    List<FilmDetailsDTO> findAvailableFilmsByTitlePart(String titlePart) throws FilmNotFoundException;

    /**
     * Finds all films that released between two dates
     * @param dateFromText
     * @param dateToText
     * @return a list of films
     * @throws FilmNotFoundException
     */
    List<FilmDetailsDTO> findFilmsByReleaseDate(String dateFromText, String dateToText)
            throws FilmNotFoundException;

    /**
     * Finds all the films that have a Genre with a genreName that contains the given genrePart
     * @param genreNamePart
     * @return a list of films
     */
    List<FilmDetailsDTO> findFilmsByGenre(String genreNamePart);

    /**
     * Finds all films that have the given actor details
     * @param actorFirstNamePart
     * @param actorLastNamePart
     * @return the list of films
     */
    List<FilmDetailsDTO> findFilmsByActor(String actorFirstNamePart, String actorLastNamePart);
    
    void validateNewFilm(FilmDetailsDTO filmDetailsDTO) throws FilmDataIncompleteException, FilmDuplicateException;

    /**
     * Gets the best selling films according to rentals
     * @return a list of films
     */
    List<FilmRentalCountDTO> getBestSellingFilms();

    /**
     * Gets the unavailable films
     * @return a list of unavailable films
     */
    List<FilmDetailsDTO> getAllUnavailableFilms();
}
