package com.accenturetraining.team1.acmedvdwebapp.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data Transfer Object
 * Used as a response body to get general info about the customer
 */

@Data
@NoArgsConstructor
public class CustomerDetailsDTO {
    private long id;
    private String firstName;
    private String lastName;
    private String address;
    private String mobileNumber;
    private String homeNumber;
    private String email;
    private String identificationNumber;
    private double debt;
    private double totalPayments;
}
