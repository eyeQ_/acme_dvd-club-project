package com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.IOException;

@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Customer already exists")
public class CustomerDuplicateException extends IOException {
    public CustomerDuplicateException(String fieldType, String fieldValue) {
        super("CustomerDuplicateException: A customer with the " + fieldType +
                " " + fieldValue + " already exists in the database");
    }
    
    public CustomerDuplicateException() {
        super("CustomerDuplicateException: A customer with the provided details already exists in the database");
    }
}
