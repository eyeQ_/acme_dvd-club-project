package com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Film already exists")
public class FilmDuplicateException extends Exception {
    public FilmDuplicateException(String titleTitle, int year) {
        super("FilmDuplicateException: A film with the title '" + titleTitle +
                "' that was released during the releaseDate " + year
                + " already exists in the database");
    }
}
