package com.accenturetraining.team1.acmedvdwebapp.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * Data Transfer Object
 * Used as a response body to get general info about the film
 */

@Data
@NoArgsConstructor
public class FilmDetailsDTO {
    private long id;
    private String title;
    private String director;
    private LocalDate releaseDate;
    private String description;
    private String actors;
    private String genres;
}
