package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.dtos.ActorDTO;
import com.accenturetraining.team1.acmedvdwebapp.model.Actor;
import com.accenturetraining.team1.acmedvdwebapp.model.Film;
import com.accenturetraining.team1.acmedvdwebapp.repositories.ActorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ActorServicesImpl implements ActorServices {
    @Autowired
    ActorRepository actorRepository;

    @Override
    public Actor registerNewActor(ActorDTO actorDTO) {
        Actor actor = actorRepository
                .getByFirstNameAndLastName(actorDTO.getFirstName(),
                        actorDTO.getLastName()).orElse(null);
        if (actor == null) {
            Actor newActor = new Actor(actorDTO.getLastName(), actorDTO.getFirstName());
            List<Film> films = new ArrayList<>();
            newActor.setFilms(films);
            actorRepository.save(newActor);
        }
        
        return actor;
    }

    @Override
    public void updateActor(Actor actor) {
        actorRepository.save(actor);
    }

//    @Override
//    public List<Actor> searchForActors(String actorNamePart) throws ActorNotFoundException {
//
//        List<Actor> actors = actorRepository.findAllByFirstNameOrLastNameContaining(actorNamePart)
//                .orElseThrow(() -> new ActorNotFoundException(actorNamePart));
//        return actors;
//    }
}
