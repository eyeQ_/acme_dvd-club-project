package com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Film Not Found") //404
public class ActorNotFoundException extends IndexOutOfBoundsException{

    public ActorNotFoundException(String actorNamePart) {

        super("ActorNotFoundException with a first or last name that contains:" + actorNamePart);
    }


}
