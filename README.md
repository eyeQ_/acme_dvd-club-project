# ACME DVD Rental Service

ACME DVD Store is an online DVD rental platform which consists of three systems;Central DVD Repository, DVD Rental Services and Reporting Services. 

## Description of the product functionality

The DVD rental platform is able to provide registration functionalities for new films. Each film may consist of different category types such as DVD and BluRay. The system can provide search functionalities to the catalogue and retrieve films depending on the user's will. The DVD Store supports rental services to customers. Customers must have an active account in order to rent a DVD or BluRay and also a copy of the corresponding film must be available. 

The system can handle DVD returns from customers and is able to manage transactional services based on DVD daily and late fees. Also, the system can retireve a list of best-selling films, a list of out of stock films and the top 20 customers based on their rentals. Finally, upon reporting services, the system is able to produce an excel file, providing weekly and monthly rental reports.

### Software Interfaces

For the development of the project, we used Intellij, a Java integrated development environment which provides great features such as code completion by analyzing the context.It supports version control systems such as Git, which we used for tracking changes in our source code during software development by using braches on GitLab.

For storing and retrieving data we used a  relational database management system, Microsoft SQL Server which can be accessed directly from the IDE.

In order to develop the application we also used various other tools, such as: 

```
Spring Framework / Spring Boot / Spring Data REST
Model Mapper Framework to handle the conversions between the entities of a Spring application using DTOs
Java Collections Framework to manipulate objects
Java Persistence API(JPA) for Mapping Java objects to database tables
Lombok Library 
Tomcat which provides an HTTP web server environment where our code can run.
Maven Project 
Data Access Objects to perform CRUD operations
Slack, Discord and Viber for team communication
Trello for organizing and managing tasks and team members.
Balsamiq and draw.io tools for creating UML Diagrams for documentation
```

### Software for testing our web application

For testing our Rest API to perfom calls we used Postman. Based on HTTP requests, the server responds with an HTTP response with JSON . 

* [Our Postman Sample Collection](https://www.getpostman.com/collections/c20cc47580e003639f28)

Our basic HTTP requests, using Postman's UI are:

```
**For Rental Services by using a request URI**

GET requests:
http://localhost:8080/customers we GET a list of customers.
http://localhost:8080/customers/{customerId} we GET the customer's information by id
http://localhost:8080/customers/details we GET a customer or list of customers based on multiple search criteria
http://localhost:8080/customers/reports/topcustomers we GET the top customers
http://localhost:8080/customers/{customerId}/films we GET the customer's rentals by id from films
http://localhost:8080/customers/{customerId}/rentals we GET the customer's rentals by id from rentals

POST requests using Responce Body:
http://localhost:8080/customers to register a customer 
http://localhost:8080/customers/{customerId}/pay to make a payment
http://localhost:8080/customers/{customerId}/films/{filmId}/rentals/{copyCategoryId} to rent a copy
http://localhost:8080/customers/returns/{copyId} to return a copy

PUT request using Responce Body:
http://localhost:8080/customers/{customerId} to update the customer's information

DELETE request:
http://localhost:8080/customers/{customerId} to delete a customer
```
```
**For DVD Repository by using a request URI**

GET requests:
http://localhost:8080/films we GET the list of all films
http://localhost:8080/films/{filmId} we GET the film's information by id
http://localhost:8080/films/unavailablefilms we GET a list with unavailable films
http://localhost:8080/films/bestsellingfilms we GET a list with the best selling films
http://localhost:8080/films/titles we GET a film or a list of films by title
http://localhost:8080/films/genres we GET a film or a list of films by genre
http://localhost:8080/films/actors we GET a film or a list of films by actor's full name
http://localhost:8080/films/dates we GET a film or list of films by giving two dates 
http://localhost:8080/films/titles/available we GET a film or list of films by title where at least one copy is available
http://localhost:8080/films/rentalsbetween we GET a report of film rentals between the given dates in .xlsx format

POST requests using Responce Body:
http://localhost:8080/films to register a new film
http://localhost:8080/films/{filmId}/copies/{copyCategoryId} to register a new copy by giving the film id and copy category

PUT request using Responce Body:
http://localhost:8080/films/{filmId} to update the film's information
```

### Known Issues - Future Work

*  When editing an already existing film, the actors and genres that are given in the request body are added to the actors and genres of the existing film, instead of replacing them.
*  Film registration validation is still inadequate . Currently (version 0.06), it is possible to register two films with the same title and release date. Our intention is to dissallow the regitration of multiple films with the same title AND release date, but still allow multiple films with the same title.
*  Currently, there are no available requests for adding, editing and deleting copy categories and general settings.
*  There is no delete request for film copies. We don't intent to add a delete request for films.
*  We intend to add a "state" attribute to every film copy that will indicate if the copy is broken, away for repair, lost, etc.
*  We intend to implement an advanced search functionality for films, using multiple search criteria.


## Built With

* [IntelliJ](https://www.jetbrains.com/idea/)
* [Spring Framework](https://spring.io/) 
* [Maven](https://maven.apache.org/) 
* [Microsoft SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-editions-express)

External libraries used through Maven dependencies:
* [Model Mapper Framework](http://modelmapper.org/)
* [Lombok](https://projectlombok.org/)
* [Apache POI](https://poi.apache.org/)
* [Gson](https://github.com/google/gson)
* [Hibernate-Java 8](https://hibernate.org/orm/releases/)



## Developers

```
Chronopoulou Danae 
Lagios Alexandros 
Papagelopoulos George 
```

## License

This project was developed under the Java training program by Accenture in April 2019. 



